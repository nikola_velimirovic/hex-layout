package hexlib;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AppModel implements Serializable, Comparable<AppModel> {

    private String mPackage;
    private String mLabel;
    private String mActivityName;

    public AppModel(String aPackage) {
        mPackage = aPackage;
    }

    public String getPackage() {
        return mPackage;
    }

    public void setPackage(String aPackage) {
        mPackage = aPackage;
    }

    public String getLabel() {
        return mLabel;
    }

    public void setLabel(String label) {
        mLabel = label;
    }

    @Override
    public int compareTo(@NonNull AppModel another) {
        return mLabel.compareTo(another.getLabel());
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o.getClass().isAssignableFrom(getClass())) {
            return o.hashCode() == hashCode();
        }
        return false;
    }

    public String getActivityName() {
        return mActivityName;
    }

    public void setActivityName(String activityName) {
        mActivityName = activityName;
    }

    @Override
    public int hashCode() {
        String s = "AppModel{ mPackage=" + mPackage + " mActivityName= " + mActivityName + ", mLabel='" + mLabel + "}";
        return s.hashCode();
    }

    @Override
    public String toString() {
        return "AppModel{" +
                "mPackage='" + mPackage + '\'' +
                ", mLabel='" + mLabel + '\'' +
                '}';
    }


}
