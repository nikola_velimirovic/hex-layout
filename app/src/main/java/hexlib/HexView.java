package hexlib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;


/**
 * Created by Nikola on 7/11/2015.
 */
public class HexView extends ImageView implements HexBitmapTransformer.BitmapTransformerCallback {

    private String mPath;
    private String mText = null;
    private boolean mPlaceholderSet = false;
    private Point mCenterPoint = new Point();
    private Paint mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mOverlayPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Rect mTextBounds;
    boolean mDrawOverlay = false;
    private Path mOverlayPath;
    private Runnable mOverRunnable = new Runnable() {
        @Override
        public void run() {
            mDrawOverlay = false;
            invalidate();
        }
    };
    private int mAdapterPosition;

    public HexView(Context context) {
        super(context);
        init();
    }

    public HexView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HexView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mTextPaint.setColor(Color.WHITE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTextPaint.setElegantTextHeight(true);
        }
        mTextPaint.setSubpixelText(true);
        float density = getResources().getDisplayMetrics().density;
        mTextPaint.setTextSize(11 * density);
        mTextPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        float shadowRadius = 1 * density;
        mTextPaint.setShadowLayer(shadowRadius, 0, 0, Color.DKGRAY);

        mOverlayPaint.setColor(Color.BLUE);
        mOverlayPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mOverlayPaint.setAlpha(120);
        mOverlayPaint.setStrokeWidth(1 * density);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);

        if (mDrawOverlay && mOverlayPath != null) {
            canvas.drawPath(mOverlayPath, mOverlayPaint);
        }

        if (mText != null) {
            //drawing text
            canvas.drawText(mText, mCenterPoint.x - (mTextBounds.right + mTextBounds.left) / 2, mCenterPoint.y - mTextBounds.bottom, mTextPaint);
        }
    }


    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            mDrawOverlay = true;
            removeCallbacks(mOverRunnable);
            postDelayed(mOverRunnable, 300);
        }
        invalidate();

        return super.onTouchEvent(event);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = getMeasuredHeight();
        int width = getMeasuredWidth();
        mCenterPoint.x = width / 2;
        mCenterPoint.y = height - height / 4;
    }

    @Override
    public void onTransformationFinished(String path, Bitmap result, Palette palette) {
        if (!TextUtils.equals(path, mPath) && result != null) {
            //fixing pallete so we read optimisation
            if (palette != null) {
                int lightVibrantColor = palette.getLightVibrantColor(Color.WHITE);
                mTextPaint.setColor(lightVibrantColor);
                mOverlayPaint.setColor(palette.getVibrantColor(Color.BLUE));
                mOverlayPaint.setAlpha(50);
                mTextPaint.setColor(Color.WHITE);
            }

            mPath = path;
            setImageBitmap(result);
            mPlaceholderSet = false;

        }
    }

    @Override
    public void setPlaceholder(Bitmap placeholder) {
        if (!mPlaceholderSet) {
            mPlaceholderSet = true;
            setImageBitmap(placeholder);
        }
    }

    @Override
    public void onPathChanged(Path path) {
        mOverlayPath = path;
    }

    public String getText() {
        return mText;
    }

    public void setText(String text) {
        mText = text;
        resetTextBounds();
    }

    public void setTextSize(int dp) {
        mTextPaint.setTextSize(dp * getResources().getDisplayMetrics().density);
        resetTextBounds();
    }

    public void setTextColor(int color) {
        mTextPaint.setColor(color);
    }

    private void resetTextBounds() {
        mTextBounds = new Rect();
        mTextPaint.getTextBounds(mText, 0, mText.length(), mTextBounds);
    }

    public int getAdapterPosition() {
        return mAdapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        mAdapterPosition = adapterPosition;
    }

}
