package hexlib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Nikola on 5/23/2015.
 */
public class HexLayout extends RelativeLayout {

    private Path hexagonPath;
    private float radius;
    private float width, height;
    private Paint mBorderPaint;
    private Paint mShadowPaint;

    public HexLayout(Context context) {
        super(context);
        init();
    }

    public HexLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HexLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2
                && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(LAYER_TYPE_SOFTWARE, null);
        }
        hexagonPath = new Path();

        mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBorderPaint.setColor(Color.RED);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(3);


        mShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mShadowPaint.setColor(Color.YELLOW);
        mShadowPaint.setShadowLayer(3, 0, 0, Color.BLACK);
    }

    private void calculatePath() {
        float triangleHeight = (float) (Math.sqrt(3) * radius / 2);
        float centerX = width / 2;
        float centerY = height / 2;

        float leftPoint = centerX - triangleHeight;
        float rightPoint = centerX + triangleHeight;

        //donja tacka
        hexagonPath.moveTo(centerX, centerY + radius);
        //donja leva tacka
        hexagonPath.lineTo(leftPoint, centerY + radius / 2);
        //gornja leva tacka
        hexagonPath.lineTo(leftPoint, centerY - radius / 2);
        //gornja tacka
        hexagonPath.lineTo(centerX, centerY - radius);
        //desna gornja tacka
        hexagonPath.lineTo(rightPoint, centerY - radius / 2);
        //desnja donja tacka
        hexagonPath.lineTo(rightPoint, centerY + radius / 2);
        //donja tacka
        hexagonPath.lineTo(centerX, centerY + radius);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.clipPath(hexagonPath);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        canvas.drawPath(hexagonPath, mShadowPaint);
        super.dispatchDraw(canvas);
        canvas.drawPath(hexagonPath, mBorderPaint);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = getMeasuredWidth();
        height = getMeasuredHeight();
        radius = height / 2;
        calculatePath();
    }

}
