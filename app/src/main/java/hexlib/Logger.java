package hexlib;


import android.util.Log;
import android.view.MotionEvent;

public class Logger {

    public static final String DEFAULT_LOG_TAG = "nikola";
    public static final String MOTION_LOG_TAG = "KMOTION";
    private static final String LOG_PREFIX = "kudos_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 30;
    private String mTag;
    private boolean mDebug = true;
    public static final Logger def;
    public static final Logger motion;

    static {
        def = Logger.get(DEFAULT_LOG_TAG);
        motion = Logger.get(MOTION_LOG_TAG);
    }

    public Logger() {
        mTag = DEFAULT_LOG_TAG;
        setDebuggable(true);
    }

    private Logger(String tag) {
        if (tag == null)
            tag = DEFAULT_LOG_TAG;
        mTag = tag;
        setDebuggable(true);
    }

    private Logger(Class tag) {
        if (tag == null) {
            mTag = DEFAULT_LOG_TAG;
        } else {
            mTag = tag.getSimpleName();
        }
        setDebuggable(true);
    }

    public void setDebuggable(boolean debuggable) {
        mDebug = debuggable;
    }

    public static String makeLogTag(String str) {
        String logTag = DEFAULT_LOG_TAG;

        if (str == null) {
            return logTag;
        }

        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            logTag = LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        } else {
            logTag = LOG_PREFIX + str;
        }
        return logTag;
    }

    public static void logMotionEventAction(MotionEvent event, Logger log) {
        //for debugging purposes
        switch (event.getAction()) {
            case MotionEvent.ACTION_CANCEL:
                log.d("ACTION_CANCEL");
                break;
            case MotionEvent.ACTION_DOWN:
                log.d("ACTION_DOWN");

                break;
            case MotionEvent.ACTION_MOVE:
                log.d("ACTION_MOVE");

                break;
            case MotionEvent.ACTION_UP:
                log.d("ACTION_UP");

                break;
            case MotionEvent.ACTION_HOVER_ENTER:
                log.d("ACTION_HOVER_ENTER");

                break;
            case MotionEvent.ACTION_HOVER_EXIT:
                log.d("ACTION_HOVER_EXIT");

                break;
            case MotionEvent.ACTION_HOVER_MOVE:
                log.d("ACTION_HOVER_MOVE");

                break;
            case MotionEvent.ACTION_MASK:
                log.d("ACTION_MASK");

                break;
            case MotionEvent.ACTION_OUTSIDE:
                log.d("ACTION_OUTSIDE");

                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                log.d("ACTION_POINTER_DOWN");

                break;
            case MotionEvent.ACTION_SCROLL:
                log.d("ACTION_SCROLL");

                break;

        }
    }

    public static Logger get() {
        return new Logger();
    }

    public static Logger get(String tag) {
        return new Logger(tag);
    }

    public static Logger get(Class clas) {
        return new Logger(clas);
    }

    public void w(String message) {
        if (mDebug)
            Log.w(mTag, message);
    }

    public void e(String message) {
        if (mDebug)
            Log.e(mTag, message);
    }

    public void e(Throwable e) {
        if (mDebug)
            Log.e(mTag, "error: ", e);
    }

    public void e(String message, Throwable e) {
        if (mDebug)
            Log.e(mTag, message, e);
    }

    public void v(String message) {
        if (mDebug)
            Log.v(mTag, message);
    }

    public void d(String message) {
        if (mDebug)
            Log.d(mTag, message);
    }

    public void i(String message) {
        if (mDebug)
            Log.i(mTag, message);
    }

}
