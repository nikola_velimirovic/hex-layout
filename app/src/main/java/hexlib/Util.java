package hexlib;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nikola on 7/13/2015.
 */
public class Util {

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static void centerDrawText(String text, Point position, Paint paint, Canvas canvas) {
        Rect textBounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, position.x - (textBounds.right + textBounds.left) / 2, position.y, paint);
    }

    /**
     * Returns list containing info about all apps on the device.
     *
     * @param context
     * @return list of the apps
     */
    public static List<AppModel> getApps(Context context) {
        PackageManager pm = context.getPackageManager();
        List<AppModel> apps = new ArrayList<>();

        List<ResolveInfo> launchableApps = getAllInstalledApplications(context);

        final Configuration config = new Configuration();
        config.locale = Locale.ENGLISH;

        for (ResolveInfo r : launchableApps) {
            String appPackage = r.activityInfo.applicationInfo.packageName;
            String activityName = r.activityInfo.name;
            AppModel app = new AppModel(appPackage);
            String label = r.loadLabel(pm).toString();
            app.setActivityName(activityName);
            app.setLabel(label);
            apps.add(app);
        }
        Collections.sort(apps);
        return apps;
    }

    public static List<ResolveInfo> getAllInstalledApplications(Context context) {
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        return context.getPackageManager().queryIntentActivities(mainIntent, 0);
    }

    public static Bitmap loadIcon(Context context, String apackage) {
        Bitmap applicationIcon = null;
        PackageManager pm = context.getPackageManager();
        try {
            applicationIcon = drawableToBitmap(pm.getApplicationIcon(apackage));
        } catch (PackageManager.NameNotFoundException e) {
            Logger.def.e("failed resolving icon for " + apackage);
        }
        return applicationIcon;
    }


}
