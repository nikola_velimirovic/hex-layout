package hexlib;

import android.content.Context;
import android.graphics.Color;

public class HexBitmapTransformerBuilder {

    private Context mContext;
    private HexBitmapTransformer.BitmapLoader mLoader;
    private int mPlaceholder;
    private int mWidth = -1;
    private int mHeight = -1;
    private int mShadowSize = 2;
    private float mDx = 0.5f;
    private float mDy = 0.5f;
    private int mShadowColor = Color.DKGRAY;
    private int mBorderSize = 1;
    private int mBorderColor = Color.BLACK;

    public HexBitmapTransformerBuilder(Context context, int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("width and height must be greater than 0");
        }

        mContext = context;
        mWidth = width;
        mHeight = height;
    }

    public HexBitmapTransformerBuilder loader(HexBitmapTransformer.BitmapLoader loader) {
        mLoader = loader;
        return this;
    }

    public HexBitmapTransformerBuilder placeholder(int placeholder) {
        mPlaceholder = placeholder;
        return this;
    }

    public HexBitmapTransformerBuilder shadow(int radius, float dx, float dy, int color) {
        mShadowSize = radius;
        mShadowColor = color;
        mDx = dx;
        mDy = dy;
        return this;
    }

    public HexBitmapTransformerBuilder border(int size, int color) {
        mBorderSize = size;
        mBorderColor = color;
        return this;
    }

    public HexBitmapTransformer create() {
        return new HexBitmapTransformer(mContext, mLoader, mPlaceholder, mWidth, mHeight, mShadowSize, mDx, mDy, mShadowColor, mBorderSize, mBorderColor);
    }
}