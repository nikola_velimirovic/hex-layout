package hexlib;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;

import com.velidev.hexagonview.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikola on 5/24/2015.
 */
public class HexLayoutManager extends RecyclerView.LayoutManager {

    public static Logger mLog = Logger.get(HexLayoutManager.class);

    protected static class HexPosition {

        int left, top, right, bottom;
        int position;

        public HexPosition(HexPosition position) {
            left = position.left;
            top = position.top;
            right = position.right;
            bottom = position.bottom;
            this.position = position.position;
        }

        public HexPosition() {
        }

        public HexPosition(int position, int left, int top, int right, int bottom) {
            this.position = position;
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }
    }

    protected int mMaxColumnCount = 4;
    protected SparseArray<HexPosition> mPositions = new SparseArray<>();
    protected float mCellHeight = -1;
    protected float mCellWidth = -1;
    protected float mVRadius;
    protected int mVScroll = 0;

    public HexLayoutManager(int maxColumnCount) {
        mMaxColumnCount = maxColumnCount;
    }

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams((int) mCellWidth, (int) mCellHeight);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        detachAndScrapAttachedViews(recycler);
        checkCellSize();
        mPositions = calculatePositions(state);
        layoutChildren(recycler, state);
    }

    private void layoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        SparseArray<View> viewCache = new SparseArray<>(getChildCount());
        List<Integer> positions = getVisiblePositions(state);

        if (getChildCount() != 0) {
            for (int i = 0; i < getChildCount(); i++) {
                final HexView child = (HexView) getChildAt(i);
                int position = child.getAdapterPosition();
                viewCache.put(position, child);
            }
        }

        for (int i = 0; i < viewCache.size(); i++) {
            View view = viewCache.valueAt(i);
            detachView(view);
        }

        for (Integer position : positions) {
            HexView view = (HexView) viewCache.get(position);

            if (view == null) {
                view = (HexView) recycler.getViewForPosition(position);
                addView(view);
                measureExactly(view);
            } else {
                attachView(view);
                viewCache.remove(position);
            }

            HexPosition hexPosition = mPositions.get(position);
            int top = hexPosition.top + mVScroll;
            int bottom = hexPosition.bottom + mVScroll;
            layoutDecorated(view, hexPosition.left, top, hexPosition.right, bottom);
        }

        for (int i = 0; i < viewCache.size(); i++) {
            View view = viewCache.valueAt(i);
            recycler.recycleView(view);
        }
    }

    /**
     * Returns position interpolated by distance list is scrolled by.
     *
     * @param index
     * @return
     */
    protected HexPosition getInterpolatedPosition(int index) {
        HexPosition position = mPositions.get(index);
        HexPosition result = new HexPosition(position);
        result.top = result.top + mVScroll;
        result.bottom = result.bottom + mVScroll;
        return result;
    }

    public HexPosition getPosition(int index) {
        return mPositions.get(index);
    }

    protected List<Integer> getVisiblePositions(RecyclerView.State state) {
        //ovo treba izmeniti da prodje kroz sve iteme i nadje one koji se nalaze u odredjenim pozicijama, mada
        float cellSpace = mCellWidth * mCellHeight;

        float vScroll = Math.abs(mVScroll);
        //ostavljamo po jedan ispred i iza
        float rowSpace = (cellSpace * mMaxColumnCount);
        float top = vScroll * getHorizontalSpace() - rowSpace;
        float bottom = (vScroll + getVerticalSpace()) * getHorizontalSpace() + rowSpace;

        //da se ne bi desilo da skida jedan po jedan item1
        int topPosition = (int) Math.max(Math.floor(top / cellSpace), 0);
        int bottomPosition = Math.min((int) Math.ceil(bottom / cellSpace), state.getItemCount() - 1);

        ArrayList<Integer> positions = new ArrayList<>();

        //TODO optimize this to get limit from bottomPos i mPositions size and than just subtract from mPositions
        for (int i = topPosition; i <= bottomPosition; i++) {
            positions.add(i);
        }

        return positions;
    }

    protected void measureExactly(View view) {
        //TODO optimize this to check if child is already measured properly since size of elements is the same
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        if (measuredHeight != mCellHeight || measuredWidth != mCellWidth) {
            final int widthSpec = View.MeasureSpec.makeMeasureSpec((int) mCellWidth, View.MeasureSpec.EXACTLY);
            final int heightSpec = View.MeasureSpec.makeMeasureSpec((int) mCellHeight, View.MeasureSpec.EXACTLY);
            view.measure(widthSpec, heightSpec);
        }
    }

    public void checkCellSize() {
        float horizontalSpace = getHorizontalSpace();
        if (mCellWidth == -1) {
            mCellWidth = (horizontalSpace / mMaxColumnCount) * 0.9f;
            mCellHeight = (horizontalSpace / mMaxColumnCount);
            mVRadius = mCellHeight / 2;
        }
    }

    protected SparseArray<HexPosition> calculatePositions(RecyclerView.State state) {
        SparseArray<HexPosition> positions = new SparseArray<>();
        int itemCount = state.getItemCount();
        if (itemCount == 0) return positions;

        //uzi red
        int minColumnCount = mMaxColumnCount - 1;

        float rowWidth = mMaxColumnCount * mCellWidth;
        float leftOffset = (getWidth() - rowWidth) / 2;

        int row = 0;
        int position = 0;

        while (itemCount > 0) {
            float vOffset = 0;
            float hOffset = 0;

            if (row > 0) {
                //za pola radiusa ide gore kako bi ulazili view-i jedan u drugi
                vOffset = (mVRadius / 2) * -1;
            }

            //how many items goes in this row
            int itemsInARow = mMaxColumnCount;
            if (row % 2 == 0) {
                itemsInARow = minColumnCount;
                hOffset = mCellWidth / 2;
            }

            int itemsToAdd = Math.min(itemsInARow, itemCount);

            //ovo je ako imam 3 ili vise elemenata u redu
            for (int i = 0; i < itemsToAdd; i++) {
                //kako to odrediti
                int x = (int) (mCellWidth * i + hOffset + leftOffset);
                int y = (int) (mCellHeight * row + row * vOffset + row * mVRadius * 0.075f);
                HexPosition rect = new HexPosition();
                rect.left = x;
                rect.top = y;
                rect.right = (int) (x + mCellWidth);
                rect.bottom = (int) (y + mCellHeight);
                rect.position = position;

                positions.put(position, rect);
                position++;
            }

            itemCount -= itemsToAdd;
            row++;
        }
        return positions;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        //sanity check
        if (getChildCount() == 0) {
            return 0;
        }

        HexPosition first = getInterpolatedPosition(0);
        HexPosition last = getInterpolatedPosition(state.getItemCount() - 1);

        int scroll = dy * -1;

        //if we are scrolling down
        if (scroll < 0) {
            if (first.top > 0) {
                return 0;
            }

            if (last.bottom <= getVerticalSpace()) {
                HexPosition lastPos = mPositions.get(state.getItemCount() - 1);
                mVScroll = (lastPos.bottom - getVerticalSpace()) * -1;
                layoutChildren(recycler, state);
                return 0;
            }

            //ako je distanca izmedju
            int distanceToBottom = last.bottom - getVerticalSpace();
            if (dy > distanceToBottom) {
                dy = distanceToBottom;
                scroll = dy * -1;
            }
        } else {
            if (first.top > dy) {
                dy = first.top;
                scroll = dy * -1;
            }
        }

//        mLog.i("increasing mVScroll from: " + mVScroll + " by: " + scroll);
        mVScroll += scroll;
        mVScroll = Math.min(0, mVScroll);

        offsetChildrenVertical(scroll);
        layoutChildren(recycler, state);

        return dy;
    }

    @Override
    public View findViewByPosition(int position) {
        super.findViewByPosition(position);
        final int childCount = getChildCount();
        //just sanity check
        if (childCount == 0) {
            return null;
        }

        final int firstChild = getPosition(getChildAt(0));
        final int viewPosition = position - firstChild;
        if (viewPosition >= 0 && viewPosition < childCount) {
            return getChildAt(viewPosition);
        }
        return null;
    }

    @Override
    public boolean supportsPredictiveItemAnimations() {
        return false;
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }

    @Override
    public boolean canScrollVertically() {
        return true;
    }

    /**
     * Returns width of a list without horizontal padding.
     *
     * @return width in pixels without horizontal padding
     */
    protected int getHorizontalSpace() {
        return getWidth() - getPaddingRight() - getPaddingLeft();
    }

    /**
     * Returns height of view excluding vertical padding.
     *
     * @return height in pixels without vertical padding
     */
    protected int getVerticalSpace() {
        return getHeight() - getPaddingBottom() - getPaddingTop();
    }

    public float getCellHeight() {
        return mCellHeight;
    }

    public float getCellWidth() {
        return mCellWidth;
    }

}
