package hexlib;

import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikola on 7/27/2015.
 */
public class GroupedHexLayoutManager extends HexLayoutManager {

    public static int TYPE_GROUP_INDICATOR = 0;
    public static int TYPE_ITEM = 1;
    private RecyclerView.Adapter mAdapter;

    public GroupedHexLayoutManager(int maxColumnCount) {
        super(maxColumnCount);
    }


    protected SparseArray<HexPosition> calculatePositions(RecyclerView.State state) {
        SparseArray<HexPosition> positions = new SparseArray<>();
        int itemCount = state.getItemCount();
        if (itemCount == 0) return positions;

        //uzi red
        int minColumnCount = mMaxColumnCount - 1;

        float rowWidth = mMaxColumnCount * mCellWidth;
        float leftOffset = (getWidth() - rowWidth) / 2;

        int row = 0;
        int groupRow = 0;
        int position = 0;
        int groupId = -1;

        while (itemCount > 0) {
            boolean rowReset = false;
            float vOffset = 0;
            float hOffset = 0;

            if (row > 0) {
                //za pola radiusa ide gore kako bi ulazili view-i jedan u drugi
                vOffset = (mVRadius / 2) * -1;
            }

            //how many items goes in this row
            int itemsInARow = mMaxColumnCount;
            if (groupRow % 2 == 0) {
                itemsInARow = minColumnCount;
                hOffset = mCellWidth / 2;
            }

            int itemsToAdd = Math.min(itemsInARow, itemCount);

            //TODO ovde proci kroz ovo i videti je li neki od view-a type indicator
            for (int i = 0; i < itemsToAdd; i++) {
                int pos = positions.size() + i;
                int viewType = mAdapter.getItemViewType(pos);
                if (viewType == TYPE_GROUP_INDICATOR) {
                    if (i == 0) {
                        groupId++;
                        hOffset = 0;
                    }
                    i = Math.max(1, i);
                    groupRow = 0;
                    rowReset = true;
                    itemsToAdd = i;
                    break;
                }
            }

            //ovo je ako imam 3 ili vise elemenata u redu
            for (int i = 0; i < itemsToAdd; i++) {
                //kako to odrediti
                int x = (int) (mCellWidth * i + hOffset + leftOffset);
                int y = (int) (mCellHeight * row + row * vOffset + row * mVRadius * 0.075f + groupId * 60);
                HexPosition rect = new HexPosition();
                rect.left = x;
                rect.top = y;
                rect.right = (int) (x + mCellWidth);
                rect.bottom = (int) (y + mCellHeight);
                rect.position = position;
                positions.put(position, rect);
                position++;
            }

            itemCount -= itemsToAdd;
            row++;
            //ako smo tek resetovali red ne treba da ga odma inkrementujemo jer ce otici na 1
            if (!rowReset)
                groupRow++;
        }
        return positions;
    }


//    @Override
//    protected SparseArray<HexPosition> calculatePositions(RecyclerView.State state) {
//        //ovo je sve
//        SparseArray<HexPosition> positions = new SparseArray<>();
//
//        //uzi red
//        int minColumnCount = mMaxColumnCount - 1;
//
//        int rowWidth = (int) (mMaxColumnCount * mCellWidth);
//        int padding = (getWidth() - rowWidth) / 2;
//
//        float listBottom = 0;
//        boolean longerRow = true;
//        int cellHeight = (int) mCellHeight;
//        int cellWidth = (int) mCellWidth;
//        int column = 0;
//        int columns = minColumnCount;
//        int row = 0;
//        for (int position = 0; position < state.getItemCount(); position++) {
//            int viewType = mAdapter.getItemViewType(position);
//            //TODO sad sortiranje :P
//            float left, top, bottom, right;
//
//            String type = null;
//            //ako je indicator ide skroz levo
//            if (viewType == TYPE_GROUP_INDICATOR) {
//                if (listBottom > 0) {
//                    listBottom = listBottom + padding * 2;
//                } else {
//                    listBottom = listBottom + padding;
//                }
//
//                type = "indicator";
//                top = listBottom;
//                left = padding;
//                right = padding + mCellWidth;
//                longerRow = false;
//                column = 0;
//                columns = minColumnCount;
//                bottom = top + cellHeight;
//                listBottom = bottom;
//                row = 0;
//            } else {
//                if(column == 0)
//                type = "item";
//                if (column >= columns) {
//                    column = 0;
//                    longerRow = !longerRow;
//                    columns = longerRow ? mMaxColumnCount : minColumnCount;
//                    listBottom = listBottom + cellHeight;
//                    row++;
//                }
//                int vOffset = 0;
//                if (row > 0) {
//                    //za pola radiusa ide gore kako bi ulazili view-i jedan u drugi
//                    vOffset = (int) ((mVRadius / 2) * -1);
//                }
//
//                int hOffset = longerRow ? 0 : cellWidth / 2;
//                top = listBottom + row * vOffset;// + row * mVRadius * 0.075f;
//                left = column * cellWidth + hOffset  + padding * 1.5f;
//                right = left + cellWidth;
//                bottom = top + cellHeight;
//                column++;
//            }
//            mLog.d(type + " lb: " + listBottom + " column: " + column + " top: " + top + " bottom: " + bottom + " right: " + right + " left: " + left);
//
//            positions.put(position, new HexPosition(position, (int) left, (int) top, (int) right, (int) bottom));
//
//        }
//        return positions;
//    }

    @Override
    protected List<Integer> getVisiblePositions(RecyclerView.State state) {

        //kako ovo ?
        /*

            znaci ako je top od ovog sranja manje od

         */
        float scrollTop = Math.abs(mVScroll);
        float scrollBottom = scrollTop + getVerticalSpace();
        ArrayList<Integer> positions = new ArrayList<>();

        for (int i = 0; i < mPositions.size(); i++) {
            HexPosition position = mPositions.valueAt(i);
            int top = position.top;
            int bottom = position.bottom;
            if (
                    (top >= scrollTop && top < scrollBottom)
                            ||
                            (top < scrollTop && bottom > scrollTop)
                            ||
                            (top < scrollBottom && bottom > scrollBottom)
                    ) {
                positions.add(position.position);
            }

        }
        return positions;
    }

    @Override
    public void onAdapterChanged(RecyclerView.Adapter oldAdapter, RecyclerView.Adapter newAdapter) {
        mAdapter = newAdapter;
    }
}
