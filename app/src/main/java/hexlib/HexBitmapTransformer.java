package hexlib;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.LruCache;

import com.velidev.worker.BackgroundWorkRequestHandler;
import com.velidev.worker.BackgroundWorkRequestListener;
import com.velidev.worker.Worker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikola on 7/22/2015.
 */
public class HexBitmapTransformer {

    public interface BitmapLoader {

        Bitmap load(String path, int width, int height);

    }

    public interface BitmapTransformerCallback {

        void onTransformationFinished(String path, Bitmap result, Palette palette);

        void setPlaceholder(Bitmap placeholder);

        void onPathChanged(Path path);

        int getId();

    }

    private BitmapLoader mLoader;
    private Logger mLog = Logger.get(getClass());
    private LruCache<String, Bitmap> mMemoryCache;
    private Map<String, Palette> mPaletteCache = new HashMap<>();
    private final int mRadius;
    private final Path mHexagonPath;
    private final Path mBorderPath;
    private final Paint mBorderPaint;
    private final Paint mShadowPaint;
    private final int mShadowColor;
    private int mBorderSize = 0;
    private final BackgroundWorkRequestHandler<String, Bitmap> mWorkHandler;
    private final Handler mHandler;
    private int mWidth;
    private int mHeight;
    private int mShadowSize = 0;
    private Bitmap mMask;
    private int mRequested = 0;
    private int mServed = 0;
    private Bitmap mPlaceholder;

    private class BitmapWorker implements Worker<String, Bitmap> {

        @Override
        public Bitmap doWork(String path, Object op) {
            mLog.d("do work > " + path + " payload: " + op);
            if (TextUtils.isEmpty(path)) {
                return null;
            }

            Bitmap fromMemCache = getBitmapFromMemCache(path);
            if (fromMemCache != null) {
                return fromMemCache;
            }

            Bitmap input = mLoader.load(path, mWidth, mHeight);

            if (input == null)
                return null;

            Palette palette = Palette.from(input).generate();
            Bitmap result = transformBitmap(input, palette);
//            input.recycle();
            addBitmapToMemoryCache(path, result);
            mPaletteCache.put(path, palette);
            return result;
        }
    }

    private Bitmap transformBitmap(Bitmap input, Palette palette) {
        Bitmap result;
        Bitmap icon = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(icon);

        Paint paint = new Paint();
        Paint palettePaint = new Paint();
        //cliping of image
        Paint maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        Rect src = new Rect();
        src.left = 0;
        src.top = 0;
        src.right = input.getWidth();
        src.bottom = input.getHeight();

        int size = mWidth >= mHeight ? mWidth : mHeight;
        int diff = Math.abs(mWidth - mHeight) * -1;
        int vibrantColor = -1;
        if (palette != null) {
            vibrantColor = palette.getVibrantColor(Color.BLACK);
        }
        if (vibrantColor != -1) {
            palettePaint.setColor(vibrantColor);
            mBorderPaint.setColor(vibrantColor);
            mBorderPaint.setAlpha(50);
            canvas.drawColor(vibrantColor);
        }

        Rect dst = new Rect();
        dst.left = diff;
        dst.top = diff;
        dst.right = size;
        dst.bottom = size;

        Rect mSrc = new Rect();
        mSrc.left = 0;
        mSrc.top = 0;
        mSrc.right = mMask.getWidth();
        mSrc.bottom = mMask.getHeight();

        Rect mDst = new Rect();
        mDst.left = 0;
        mDst.top = 0;
        mDst.right = mWidth;
        mDst.bottom = mHeight;

        canvas.drawBitmap(input, src, dst, paint);
        if (vibrantColor != -1) {
            float x = mWidth / 2;
            palettePaint.setShader(new LinearGradient(x, 0f, x, (float) mHeight, Color.TRANSPARENT, vibrantColor, Shader.TileMode.CLAMP));
            canvas.drawRect(0, 0, mWidth, mHeight, palettePaint);
        }

        canvas.drawBitmap(mMask, mSrc, mDst, maskPaint);
        if (mBorderSize > 0)
            canvas.drawPath(mBorderPath, mBorderPaint);

        if (mShadowColor != Color.TRANSPARENT && mShadowSize > 0) {
            //drawing shadow
            result = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            canvas = new Canvas(result);
            canvas.drawPath(mBorderPath, mShadowPaint);
            canvas.drawBitmap(icon, 0, 0, paint);
        } else {
            result = icon;
        }
        return result;
    }

    HexBitmapTransformer(Context context, BitmapLoader loader, int placeholder, int width, int height, int shadowSize, float sdx, float sdy, int scolor, int borderSize, int borderColor) {
        mLoader = loader;
        mLog.i("initializing BitmapTransformer w: " + width + " h: " + height);
        mHandler = new Handler();
        mWidth = width;
        mHeight = height;
        mRadius = mHeight / 2;
        mHexagonPath = new Path();
        mBorderPath = new Path();
        float density = context.getResources().getDisplayMetrics().density;
        mBorderSize = (int) (borderSize * density);
        mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBorderPaint.setColor(borderColor);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(mBorderSize);
        mShadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mShadowColor = scolor;
        mShadowSize = (int) (shadowSize * density);
        mShadowPaint.setColor(mShadowColor);
        int sDx = (int) (sdx * density);
        int sDy = (int) (sdy * density);
        mShadowPaint.setShadowLayer(mShadowSize, sDx, sDy, mShadowColor);
        calculatePath();
        createMask();
        if (placeholder > 0)
            createPlaceholder(context, placeholder);
        mWorkHandler = new BackgroundWorkRequestHandler<>();
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }

            @Override
            protected void entryRemoved(boolean evicted, String key, Bitmap oldValue, Bitmap newValue) {
                if (mPaletteCache != null)
                    mPaletteCache.remove(key);
            }
        };
    }

    private void createPlaceholder(Context context, int placeholder) {
        Drawable drawable = context.getResources().getDrawable(placeholder);
        Bitmap bitmap = Util.drawableToBitmap(drawable);
        mPlaceholder = transformBitmap(bitmap, Palette.generate(bitmap));
        bitmap.recycle();
    }

    public void load(final BitmapTransformerCallback callback, String path) {
        //TODO set placeholder
        mLog.i("load id: " + callback.getId() + " path: " + path);
        callback.onPathChanged(mBorderPath);
        mRequested++;
        Bitmap fromMemCache = getBitmapFromMemCache(path);
        if (fromMemCache != null) {
            mLog.i("recovered from cache " + path);
            callback.onTransformationFinished(path, fromMemCache, mPaletteCache.get(path));
            mServed++;
            return;
        } else {
            if (mPlaceholder != null)
                callback.setPlaceholder(mPlaceholder);
        }

        mWorkHandler.request(new BitmapWorker(), new BackgroundWorkRequestListener<String, Bitmap>() {

            @Override
            public void onResult(final String path, final Bitmap bitmap, Object o) {
                //TODO check if path matches the one view needs
                mLog.w("onResult > id: " + callback.getId() + " path: " + path + " bitmap: " + bitmap);
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onTransformationFinished(path, bitmap, mPaletteCache.get(path));
                        mServed++;
                        mLog.d("requested: " + mRequested + " served: " + mServed);
                    }
                });
            }

            @Override
            public int getId() {
                return callback.getId();
            }
        }, path, callback.getId());
        mLog.d("requested: " + mRequested + " served: " + mServed);
    }

    public void setPlaceholder(final BitmapTransformerCallback callback){
        if (mPlaceholder != null)
            callback.setPlaceholder(mPlaceholder);
    }

    private void calculatePath() {
        float triangleHeight = (float) (Math.sqrt(3) * mRadius / 2);
        float centerX = mWidth / 2;
        float centerY = mHeight / 2;

        float leftPoint = centerX - triangleHeight;
        float rightPoint = centerX + triangleHeight;

        //donja tacka
        mHexagonPath.moveTo(centerX, centerY + mRadius - mShadowSize);
        //donja leva tacka
        mHexagonPath.lineTo(leftPoint + mShadowSize, centerY + mRadius / 2);
        //gornja leva tacka
        mHexagonPath.lineTo(leftPoint + mShadowSize, centerY - mRadius / 2);
        //gornja tacka
        mHexagonPath.lineTo(centerX, centerY - mRadius + mShadowSize);
        //desna gornja tacka
        mHexagonPath.lineTo(rightPoint - mShadowSize, centerY - mRadius / 2);
        //desnja donja tacka
        mHexagonPath.lineTo(rightPoint - mShadowSize, centerY + mRadius / 2);
        //donja tacka
        mHexagonPath.lineTo(centerX, centerY + mRadius - mShadowSize);
        mHexagonPath.setFillType(Path.FillType.INVERSE_EVEN_ODD);

        mBorderPath.set(mHexagonPath);
        mBorderPath.setFillType(Path.FillType.EVEN_ODD);
    }

    private void createMask() {
        Bitmap result = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLACK);
        canvas.drawPath(mHexagonPath, paint);
        mMask = result;
    }

    public void onDestroy() {
        if (mMask != null) mMask.recycle();
        mMask = null;
    }

    private synchronized void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private synchronized Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

}
