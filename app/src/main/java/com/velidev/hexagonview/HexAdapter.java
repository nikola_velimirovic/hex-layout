package com.velidev.hexagonview;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hexlib.AppModel;
import hexlib.HexBitmapTransformer;
import hexlib.HexBitmapTransformerBuilder;
import hexlib.HexView;
import hexlib.Util;

/**
 * Created by Nikola on 5/24/2015.
 */
public class HexAdapter extends RecyclerView.Adapter<HexAdapter.HexViewHolder> {

    private final List<AppModel> mApps;
    Logger log = Logger.get(getClass());
    Context mContext;
    private int created = 0;
    private HexBitmapTransformer mHexBitmapTransformer;
    private int mLastHexId = 0;

    public HexAdapter(final Context context, int cellWidth, int cellHeight) {
        mContext = context;
        setHasStableIds(true);
        mApps = Util.getApps(context);

        mHexBitmapTransformer = new HexBitmapTransformerBuilder(context, cellWidth, cellHeight).loader(new HexBitmapTransformer.BitmapLoader() {
            @Override
            public Bitmap load(String path, int width, int height) {
                return Util.loadIcon(mContext, path);
            }
        }).placeholder(R.drawable.ic_launcher).create();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public HexViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        mLastHexId++;
        HexView hexView = new HexView(mContext);
        hexView.setId(mLastHexId);
        created++;
        log.d("creating view holder, sum: " + created);
        return new HexViewHolder(hexView);
    }

    @Override
    public void onBindViewHolder(HexViewHolder hexViewHolder, int position) {
        AppModel appModel = mApps.get(position);
        final HexView hexView = (HexView) hexViewHolder.itemView;
        hexView.setText(appModel.getLabel());
        mHexBitmapTransformer.load(hexView, appModel.getPackage());
    }

    @Override
    public int getItemCount() {
        return mApps.size();
    }

    public static class HexViewHolder extends RecyclerView.ViewHolder {

        public HexViewHolder(View itemView) {
            super(itemView);
        }
    }
}
