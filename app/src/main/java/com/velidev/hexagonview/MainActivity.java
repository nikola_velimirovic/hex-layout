package com.velidev.hexagonview;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import hexlib.GroupedHexLayoutManager;
import hexlib.HexLayoutManager;


public class MainActivity extends ActionBarActivity {

    RecyclerView mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mList = (RecyclerView) findViewById(R.id.list);

        mList.post(new Runnable() {
            @Override
            public void run() {
                GroupedHexLayoutManager hexLayoutManager = new GroupedHexLayoutManager(5);
                mList.setLayoutManager(hexLayoutManager);
                hexLayoutManager.checkCellSize();
                HexGroupedAdapter hexAdapter = new HexGroupedAdapter(MainActivity.this, (int) hexLayoutManager.getCellWidth(), (int) hexLayoutManager.getCellHeight());
                mList.setAdapter(hexAdapter);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
