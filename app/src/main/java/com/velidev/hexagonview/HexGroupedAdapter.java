package com.velidev.hexagonview;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hexlib.AppModel;
import hexlib.GroupedHexLayoutManager;
import hexlib.HexBitmapTransformer;
import hexlib.HexBitmapTransformerBuilder;
import hexlib.HexLayoutManager;
import hexlib.HexView;
import hexlib.Util;

/**
 * Created by Nikola on 7/31/2015.
 */
public class HexGroupedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final ArrayList<AllAppsItem> mItems;
    private Logger mLog = Logger.get(getClass());

    public static class AllAppsItem {

        public AppModel model;
        public final int type;
        public final String group;

        public AllAppsItem(int type, String group) {
            this.type = type;
            this.group = group;
        }

        public AllAppsItem(AppModel model, String group) {
            this.group = group;
            type = GroupedHexLayoutManager.TYPE_ITEM;
            this.model = model;
        }

        public AllAppsItem(String group) {
            type = GroupedHexLayoutManager.TYPE_GROUP_INDICATOR;
            //TODO unhardcode this
            if (TextUtils.isDigitsOnly(group))
                group = "#";
            this.group = group;
        }
    }

    private final Context mContext;
    private List<AppModel> mApps;
    private HexBitmapTransformer mHexBitmapTransformer;
    private int mLastHexId = 0;

    public HexGroupedAdapter(final Context context, int cellWidth, int cellHeight) {
        mContext = context;
        setHasStableIds(true);

        mItems = new ArrayList<>();
        mApps = Util.getApps(context);
        Iterator<AppModel> iterator = mApps.iterator();
        //this is how we can differentiate groups
        String lastLetter = null;
        //just a indicator to know whether we are setting up first group
        while (iterator.hasNext()) {
            AppModel appModel = iterator.next();
            //just in case...
            if (!TextUtils.isEmpty(appModel.getLabel())) {
                String fl = TextUtils.substring(appModel.getLabel(), 0, 1);
                //if first letter of current appmodel and lastletter doesn't match we are in
                // different group
                if (!TextUtils.equals(fl, lastLetter)) {
                    mItems.add(new AllAppsItem(fl));
                }

                mItems.add(new AllAppsItem(appModel, fl));
                lastLetter = fl;
            }
        }

        mHexBitmapTransformer = new HexBitmapTransformerBuilder(context, cellWidth, cellHeight).loader(new HexBitmapTransformer.BitmapLoader() {
            @Override
            public Bitmap load(String path, int width, int height) {
                return Util.loadIcon(mContext, path);
            }
        }).placeholder(R.drawable.ic_launcher).create();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mLastHexId++;
        HexView hexView = new HexView(mContext);
        hexView.setId(mLastHexId);
        if (viewType == GroupedHexLayoutManager.TYPE_ITEM) {
            return new HexViewHolder(hexView);
        } else {
            return new HexIndicatorViewHolder(hexView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HexLayoutManager.mLog.v("onBind " + position);
        int viewType = getItemViewType(position);
        final HexView hexView = (HexView) holder.itemView;
        hexView.setAdapterPosition(position);
        if (viewType == GroupedHexLayoutManager.TYPE_GROUP_INDICATOR) {
            AllAppsItem item = mItems.get(position);
            String group = item.group.toUpperCase();
            hexView.setText(group);
            mLog.d("setting up position > " + position + " type indicator group > " + group);
            mHexBitmapTransformer.setPlaceholder(hexView);
        } else if (viewType == GroupedHexLayoutManager.TYPE_ITEM) {
            AllAppsItem item = mItems.get(position);
            String label = item.model.getLabel();
            hexView.setText(label);
            mHexBitmapTransformer.load(hexView, item.model.getPackage());
            mLog.d("setting up position > " + position + " type item label > " + label);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).type;
    }

    public static class HexViewHolder extends RecyclerView.ViewHolder {

        public HexViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class HexIndicatorViewHolder extends RecyclerView.ViewHolder {

        public HexIndicatorViewHolder(View itemView) {
            super(itemView);
        }
    }
}
